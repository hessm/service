package api

import (
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/issuance-service/well_known"
	"sync"
)

var wellKnownApi *well_known.WellKnown
var log *logr.Logger

func Listen(offerTopic string, preAuthTopic string, wellKnown *well_known.WellKnown, logger *logr.Logger) {
	wellKnownApi = wellKnown
	log = logger

	var wg sync.WaitGroup

	wg.Add(1)
	log.Info("start messaging")
	go startMessaging(offerTopic, preAuthTopic, &wg)

	wg.Wait()
}
