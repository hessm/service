package config

import (
	"fmt"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/config"
)

type IssuanceServiceConfig struct {
	config.BaseConfig `mapstructure:",squash"`
	OfferTopic        string `mapstructure:"offerTopic"`
	PreAuthTopic      string `mapstructure:"preAuthTopic"`
	WellKnownTopic    string `mapstructure:"wellKnownTopic"`
}

func LoadConfig() (*IssuanceServiceConfig, error) {
	var currentIssuanceServiceConfig IssuanceServiceConfig

	if err := config.LoadConfig("ISSUANCE", &currentIssuanceServiceConfig, nil); err != nil {
		return nil, fmt.Errorf("could not load config: %w", err)
	}
	return &currentIssuanceServiceConfig, nil
}
