package entity

type CredentialOfferObject struct {
	CredentialIssuer string `json:"credential_issuer"`
	Credentials      any    `json:"credentials"`
	Grants           Grants `json:"grants"`
}

type Grants struct {
	AuthorizationCode AuthorizationCode `json:"authorization_code"`
	PreAuthorizedCode PreAuthorizedCode `json:"urn:ietf:params:oauth:grant-type:pre-authorized_code"`
}

type PreAuthorizedCode struct {
	Code        string `json:"pre-authorized_code"`
	PinRequired bool   `json:"user_pin_required"`
}

type AuthorizationCode struct {
	IssuerState string `json:"issuer_state"`
}

func NewCredentialOfferObject(issuer string, credentialType string, authCode string, pinRequired bool) *CredentialOfferObject {
	grants := Grants{
		PreAuthorizedCode: PreAuthorizedCode{
			Code:        authCode,
			PinRequired: pinRequired,
		},
	}

	return &CredentialOfferObject{
		CredentialIssuer: issuer,
		Credentials:      credentialType,
		Grants:           grants,
	}
}
