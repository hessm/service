package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/issuance-service/api"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/issuance-service/config"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/issuance-service/well_known"
)

func main() {
	currentConfig, err := config.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}

	logger, err := logr.New(currentConfig.LogLevel, currentConfig.IsDev, nil)
	if err != nil {
		log.Fatal(err)
	}

	reqClient, err := cloudeventprovider.NewClient(cloudeventprovider.Req, currentConfig.WellKnownTopic)
	if err != nil {
		log.Fatal(err)
	}

	wellKnownApi, err := well_known.New(reqClient, logger)
	if err != nil {
		log.Fatal(err)
	}
	defer wellKnownApi.Close()

	api.Listen(currentConfig.OfferTopic, currentConfig.PreAuthTopic, wellKnownApi, logger)
}
